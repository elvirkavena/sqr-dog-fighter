[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=Danilum_sqr-dog-fighter&metric=code_smells)](https://sonarcloud.io/dashboard?id=Danilum_sqr-dog-fighter) [![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=Danilum_sqr-dog-fighter&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=Danilum_sqr-dog-fighter) [![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=Danilum_sqr-dog-fighter&metric=ncloc)](https://sonarcloud.io/dashboard?id=Danilum_sqr-dog-fighter) [![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=Danilum_sqr-dog-fighter&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=Danilum_sqr-dog-fighter) [![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=Danilum_sqr-dog-fighter&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=Danilum_sqr-dog-fighter) [![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=Danilum_sqr-dog-fighter&metric=security_rating)](https://sonarcloud.io/dashboard?id=Danilum_sqr-dog-fighter) [![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=Danilum_sqr-dog-fighter&metric=sqale_index)](https://sonarcloud.io/dashboard?id=Danilum_sqr-dog-fighter) [![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=Danilum_sqr-dog-fighter&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=sDanilum_sqr-dog-fighter)

## Project description 

A web application where users:
- can choose which dog is the nicer boy
- add dogs to the competition themselves
- vote for dogs in the fights
- see Rating of the all dogs

## API
Used external api https://dog.ceo/dog-api/

## Features
1. find by breed (find dog that you want to add to voting)
2. add to fight (add this dog to the voting)
3. get pair (get pair of dogs for future voting process)
4. vote (choose better dog from the pair)
5. rating (see the rating of all dogs)

## Testing methods

- Unit testing
- Integration testing
- Static analysis
- UI testing
- Exploratory testing tours
- Stress testing





