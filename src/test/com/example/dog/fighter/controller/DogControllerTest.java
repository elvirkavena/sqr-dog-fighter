package com.example.dog.fighter.controller;

import com.example.dog.fighter.DogApiClient;
import com.example.dog.fighter.DogDTO;
import com.example.dog.fighter.ImagesDTO;
import com.example.dog.fighter.controller.DogController;
import com.example.dog.fighter.entity.Dog;
import com.example.dog.fighter.repository.DogRepository;
import com.example.dog.fighter.service.DogService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(DogController.class)
@ActiveProfiles("test")
public class DogControllerTest {

    @MockBean
    private DogService dogService;

    @MockBean
    private DogApiClient dogApiClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Not correct breed test")
    void testFindByBreed() throws Exception {

        when(dogApiClient.getImages("hounddddd")).thenReturn(new ImagesDTO("error","NOTsuccess"));

        mockMvc.perform(get("/find").param("breedName", "houndddd"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rating").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.image").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.breed").value(IsNull.nullValue()));

    }

    @Test
    @DisplayName("Correct breed test")
    void testFindByBreed2() throws Exception {

        when(dogApiClient.getImages("hound")).thenReturn(new ImagesDTO("imageUrl","success"));

        mockMvc.perform(get("/find").param("breedName", "hound"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rating", is(0)))
                .andExpect(jsonPath("$.image", is("imageUrl")))
                .andExpect(jsonPath("$.breed", is("hound")));

    }

    private static final MediaType APPLICATION_JSON_UTF8 =
            new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(),
                    StandardCharsets.UTF_8);

    @Test
    @DisplayName("Correct adds to fight test")
    void testAddToFight() throws Exception {

        DogDTO dog = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
        when(dogService.existsByImage(dog.getImage())).thenReturn(false);

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog))
                .content(objectMapper.writeValueAsString(dog))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        verify(dogService).createDog(dog);

    }

    @Test
    @DisplayName("Add to fight with throw exception test")
    void testAddToFight2() throws Exception {

        DogDTO dog = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
        when(dogService.existsByImage(anyString())).thenReturn(true);

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog))
                .content(objectMapper.writeValueAsString(dog))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isImUsed());

        verify(dogService, times(0)).createDog(any());
    }


    @Test
    @DisplayName("get pair of <2 elements test")
    void testGetPair1() throws Exception {
        DogDTO dog1 = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
        List<DogDTO> dogs = new ArrayList<>(Arrays.asList(dog1));

        when(dogService.getListOfAllDogs()).thenReturn(dogs);

        mockMvc.perform(get("/getPair"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @DisplayName("get pair of 2 elements test")
    void testGetPair2() throws Exception {
        DogDTO dog1 = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
        DogDTO dog2 = new DogDTO(2, "Harvard", "http://vk.com/igorsam9", 3);
        List<DogDTO> dogs = new ArrayList<>(Arrays.asList(dog1,dog2));

        when(dogService.getListOfAllDogs()).thenReturn(dogs);

        mockMvc.perform(get("/getPair"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @DisplayName("get pair of more than 2 elements test")
    void testGetPair3() throws Exception {
        DogDTO dog1 = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
        DogDTO dog2 = new DogDTO(2, "Harvard", "http://vk.com/igorsam9", 3);
        DogDTO dog3 = new DogDTO(3, "Harvaaaaard", "http://vk.com/igorsggggam9", 3);
        List<DogDTO> dogs = new ArrayList<>(Arrays.asList(dog1,dog2,dog3));

        when(dogService.getListOfAllDogs()).thenReturn(dogs);

        mockMvc.perform(get("/getPair"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));

    }


    @Test
    @DisplayName("voting successful test")
    void testVote() throws Exception {
        when(dogService.existsById(anyInt())).thenReturn(true);

        mockMvc.perform(get("/vote").param("dogId", String.valueOf(1)))
                .andExpect(status().isOk());
        verify(dogService).existsById(1);
        verify(dogService).increaseRating(1);
    }

    @Test
    @DisplayName("voting for non existing dog test")
    void testVote2() throws Exception {
        when(dogService.existsById(4)).thenReturn(false);

        mockMvc.perform(get("/vote").param("dogId", String.valueOf(4)))
                .andExpect(status().isNotFound());

        verify(dogService, times(1)).existsById(any());;
        verify(dogService, times(0)).increaseRating(any());
    }


    @Test
    @DisplayName("get rating test")
    void testRating() throws Exception {
        DogDTO dog1 = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
        DogDTO dog2 = new DogDTO(2, "Harvard", "http://vk.com/igorsam9", 3);
        DogDTO dog3 = new DogDTO(3, "Harvaaaaard", "http://vk.com/igorsggggam9", 3);
        List<DogDTO> dogs = new ArrayList<>(Arrays.asList(dog1,dog2,dog3));
        when(dogService.getListOfAllDogs()).thenReturn(dogs);

        mockMvc.perform(get("/rating"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }
}
