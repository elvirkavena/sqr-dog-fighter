package com.example.dog.fighter.controller;

import com.example.dog.fighter.DogDTO;
import com.example.dog.fighter.repository.DogRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DogControllerIntegrationTest {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private DogRepository repository;
    @Autowired
    private MockMvc mockMvc;

    @AfterEach
    public void resetDb() {
        repository.deleteAll();
    }

    @Test
    @DisplayName("Not correct breed test")
    void testFindByBreed() throws Exception {


        mockMvc.perform(get("/find").param("breedName", "houndddd"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rating").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.image").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.breed").value(IsNull.nullValue()));

    }

    @Test
    @DisplayName("Correct breed test")
    void testFindByBreed2() throws Exception {

        String breed = "hound";

        mockMvc.perform(get("/find").param("breedName", breed))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rating", is(0)))
                .andExpect(jsonPath("$.image").value(IsNull.notNullValue()))
                .andExpect(jsonPath("$.breed", is(breed)));

    }

    @Test
    @DisplayName("Correct adds to fight test")
    void testAddToFight() throws Exception {

        DogDTO dog = new DogDTO(1, "breeeed", "image1", 2);

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog))
                .content(objectMapper.writeValueAsString(dog))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

    }

    @Test
    @DisplayName("Add to fight with throw exception test")
    void testAddToFight2() throws Exception {

        DogDTO dog = new DogDTO(1, "Igor", "image1", 2);

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog))
                .content(objectMapper.writeValueAsString(dog))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog))
                .content(objectMapper.writeValueAsString(dog))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isImUsed());
    }

    @Test
    @DisplayName("get pair of <2 elements test")
    void testGetPair1() throws Exception {
        DogDTO dog = new DogDTO(1, "Igor", "image1", 2);

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog))
                .content(objectMapper.writeValueAsString(dog))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(get("/getPair"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @DisplayName("get pair of 2 elements test")
    void testGetPair2() throws Exception {
        DogDTO dog1 = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
        DogDTO dog2 = new DogDTO(2, "Harvard", "http://vk.com/igorsam9", 3);

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog1))
                .content(objectMapper.writeValueAsString(dog1))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog2))
                .content(objectMapper.writeValueAsString(dog2))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(get("/getPair"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @DisplayName("get pair of more than 2 elements test")
    void testGetPair3() throws Exception {
        DogDTO dog1 = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
        DogDTO dog2 = new DogDTO(2, "Harvard", "http://vk.com/igorsam9", 3);
        DogDTO dog3 = new DogDTO(3, "Harvaaaaard", "http://vk.com/igorsggggam9", 3);

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog1))
                .content(objectMapper.writeValueAsString(dog1))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog2))
                .content(objectMapper.writeValueAsString(dog2))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog3))
                .content(objectMapper.writeValueAsString(dog3))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(get("/getPair"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));

    }

//    @Test
//    @DisplayName("voting successful test")
//    void testVote() throws Exception {
//
//        DogDTO dog1 = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
//
//        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog1))
//                .content(objectMapper.writeValueAsString(dog1))
//                .contentType(APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk())
//                .andExpect(content().string("true"));
//
//        mockMvc.perform(get("/vote").param("dogId", String.valueOf(1)))
//                .andExpect(status().isOk());
//
//    }

    @Test
    @DisplayName("voting for non existing dog test")
    void testVote2() throws Exception {


        mockMvc.perform(get("/vote").param("dogId", String.valueOf(4)))
                .andExpect(status().isNotFound());

    }

    @Test
    @DisplayName("get rating test")
    void testRating() throws Exception {
        DogDTO dog1 = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
        DogDTO dog2 = new DogDTO(2, "Harvard", "http://vk.com/igorsam9", 3);
        DogDTO dog3 = new DogDTO(3, "Harvaaaaard", "http://vk.com/igorsggggam9", 3);

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog1))
                .content(objectMapper.writeValueAsString(dog1))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog2))
                .content(objectMapper.writeValueAsString(dog2))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(post("/addToFight").param("DogDTO", String.valueOf(dog3))
                .content(objectMapper.writeValueAsString(dog3))
                .contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        mockMvc.perform(get("/rating"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    private static final MediaType APPLICATION_JSON_UTF8 =
            new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(),
                    StandardCharsets.UTF_8);


}
